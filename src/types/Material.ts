type Material = {
  id?: number
  name: string
  quantity: number | null
  qMin: number | null
  qMax: number | null
  created: Date
  update: Date
}

export type { Material }