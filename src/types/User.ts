type Gender = 'male' | 'female' | 'other'
type Role = 'manager' | 'staff'
type User = {
  id: number
  email: string
  password: string
  fullName: string
  gender: Gender //Male, Female ,Other
  roles: Role[] //manager,staff
}
export type { Gender, Role, User }
