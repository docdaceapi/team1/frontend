import type { Type } from './Type'
type Order = {
  id?: number
  total: string
  qty: number
  created: Date
  updated: Date
  userId: number
}

export { type Order }
