import type { Type } from './Type'

type BillPayment = {
  id?: number
  typeBill: Type
  total: number
  date: string
  time: string // datetime
}

export { type BillPayment }
