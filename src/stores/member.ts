import { ref } from 'vue'
import { defineStore } from 'pinia'
import type { Member } from '@/types/Member'
import { useLoadingStore } from './loading'
import memberService from '@/services/member'

export const useMemberStore = defineStore('member', () => {
  const loadingStore = useLoadingStore()
  const members = ref<Member[]>([])
  const initialMember: Member = {
    name: '',
    tel: ''
  }

  const editedMember = ref<Member>(JSON.parse(JSON.stringify(initialMember)))
  const currentMember = ref<Member | null>()
  
  async function getMember(id: number) {
    try {
      loadingStore.doLoad()
      const res = await memberService.getMember(id)
      editedMember.value = res.data
      loadingStore.finish()
    } catch (e) {
      loadingStore.finish()
    }
  }

  async function getMembers() {
    try {
      loadingStore.doLoad()
      const res = await memberService.getMembers()
      members.value = res.data
      loadingStore.finish()
    } catch (e) {
      loadingStore.finish()
    }
  }

  async function saveMember() {
    try {
      loadingStore.doLoad()
      const member = editedMember.value
      if (!member.id) {
        //Add new
        console.log('Post ' + JSON.stringify(member))
        const res = await memberService.addMember(member)
      } else {
        //update
        console.log('Patch ' + JSON.stringify(member))
        const res = await memberService.updateMember(member)
      }
      await getMembers()
      loadingStore.finish()
    } catch (e) {
      loadingStore.finish()
    }
  }

  async function deleteMember() {
    try {
      loadingStore.doLoad()
      const member = editedMember.value
      const res = await memberService.delMember(member)
      await getMembers()
      loadingStore.finish()
    } catch (e) {
      loadingStore.finish()
    }
  }

  function clearForm() {
    editedMember.value = JSON.parse(JSON.stringify(initialMember))
  }

  return {
    members,
    editedMember,
    loadingStore,
    initialMember,
    currentMember,
    getMember,
    getMembers,
    saveMember,
    deleteMember,
    clearForm
  }
})

// const searchMember = (tel: string) => {
//   const index = members.value.findIndex((item) => item.tel === tel)
//   if (index < 0) {
//     currentMember.value = null
//   }
//   currentMember.value = members.value[index]
// }
