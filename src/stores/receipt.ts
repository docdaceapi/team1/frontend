import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import type { ReceiptItem } from '@/types/ReceiptItem'
import type { Product } from '@/types/Product'
import type { Receipt } from '@/types/Receipt'
import { useAuthStore } from './auth'
import { useMemberStore } from './member'
import { useLoadingStore } from './loading'
import { useMessageStore } from './message'

import orderService from '@/services/order'

export const useReceiptStore = defineStore('receipt', () => {
  const authStore = useAuthStore()
  const memberStore = useMemberStore()
  const loadingStore = useLoadingStore()
  const messageStore = useMessageStore()
  const receiptDialog = ref<boolean>(false)
  const receipt = ref<Receipt>({
    id: 0,
    createdDate: new Date(),
    totalBefore: 0,
    memberDiscount: 0,
    total: 0,
    receivedAmount: 0,
    change: 0,
    paymentType: 'cash',
    userId: authStore.getCurrentUser()!.id!,
    user: authStore.getCurrentUser()!,
    memberId: 0
  })
  initReceipt()
  function initReceipt() {
    receipt.value = {
      id: 0,
      createdDate: new Date(),
      totalBefore: 0,
      memberDiscount: 0,
      total: 0,
      receivedAmount: 0,
      change: 0,
      paymentType: 'cash',
      userId: authStore.getCurrentUser()!.id!,
      user: authStore.getCurrentUser()!,
      memberId: 0
    }
  }
  const receiptItems = ref<ReceiptItem[]>([])
  function addReceiptItem(product: Product) {
    const newReceipt: ReceiptItem = {
      id: 1,
      name: product.name,
      price: product.price,
      unit: 1,
      sweet: '100%',
      productId: product.id!,
      product: product
    }
    receiptItems.value.push(newReceipt)
    calReceipt()
  }

  function removeReceiptItem(receiptItem: ReceiptItem) {
    const index = receiptItems.value.findIndex((item) => item == receiptItem)
    receiptItems.value.splice(index, 1)
    calReceipt()
  }

  function inc(item: ReceiptItem) {
    item.unit++
    calReceipt()
  }
  function dec(item: ReceiptItem) {
    if (item.unit === 1) {
      removeReceiptItem(item)
    }
    item.unit--
    calReceipt()
  }
  function calReceipt() {
    let totalBefore = 0
    for (const item of receiptItems.value) {
      totalBefore += item.price * item.unit
    }
    receipt.value.totalBefore = totalBefore

    if (memberStore.currentMember) {
      receipt.value.total = totalBefore * 0.9
      receipt.value.memberDiscount = totalBefore * 0.1
    } else {
      receipt.value.total = totalBefore
    }
    if (receipt.value.change == 0) {
      receipt.value.change = receipt.value.receivedAmount - receipt.value.total
    }
    if (receipt.value.change < 0) {
      receipt.value.change = 0
    } else {
      receipt.value.change = receipt.value.receivedAmount - receipt.value.total
    }
  }

  function showReceiptDialog() {
    receipt.value.receiptItems = receiptItems.value
    receiptDialog.value = true
  }
  function clear() {
    receiptItems.value = []
    receipt.value = {
      id: 0,
      createdDate: new Date(),
      totalBefore: 0,
      memberDiscount: 0,
      total: 0,
      receivedAmount: 0,
      change: 0,
      paymentType: 'cash',
      userId: authStore.getCurrentUser()!.id!,
      user: authStore.getCurrentUser()!,
      memberId: 0
    }
    memberStore.clear()
  }
  function updateSweetness(item: ReceiptItem, sweetnessLevel: string): void {
    item.sweet = sweetnessLevel
  }

  const order = async () => {
    try {
      loadingStore.doLoad()
      await orderService.addOrder(receipt.value!, receiptItems.value)
      initReceipt()
      clear()
      console.log(receipt.value)
      console.log(receiptItems.value)
      loadingStore.finish()
    } catch (e: any) {
      loadingStore.finish()
      messageStore.showMessage(e.message)
    }
  }

  return {
    receiptItems,
    receipt,
    receiptDialog,
    addReceiptItem,
    removeReceiptItem,
    inc,
    dec,
    calReceipt,
    showReceiptDialog,
    clear,
    updateSweetness,
    order
  }
})
