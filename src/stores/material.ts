import { useLoadingStore } from './loading'
import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import materialService from '@/services/material'
import type { Material } from '@/types/Material'

export const useMaterialStore = defineStore('material', () => {
  const loadingStore = useLoadingStore()
  const materials = ref<Material[]>([])
  const initialMaterial: Material = {
    name: '',
    quantity: null,
    qMin: null,
    qMax: null,
    created: new Date,
    update: new Date
  }
  const editedMaterial = ref<Material>(JSON.parse(JSON.stringify(initialMaterial)))

  async function getMaterial(id: number) {
    loadingStore.doLoad()
    const res = await materialService.getMaterial(id)
    editedMaterial.value = res.data
    loadingStore.finish()
  }


  async function getMaterials() {
    try {
      loadingStore.doLoad()
      const res = await materialService.getMaterials()
      materials.value = res.data
      loadingStore.finish()
    } catch (e) {
      loadingStore.finish()
    }
  }
  async function saveMaterial() {
    loadingStore.doLoad()
    const material = editedMaterial.value
    if (!material.id) {
      // Add new
      console.log('Post ' + JSON.stringify(material))
      const res = await materialService.addMaterial(material)
    } else {
      // Update
      console.log('Patch ' + JSON.stringify(material))
      const res = await materialService.updateMaterial(material)
    }

    await getMaterials()
    loadingStore.finish()
  }
  async function deleteMaterial() {
    loadingStore.doLoad()
    const material = editedMaterial.value
    const res = await materialService.delMaterial(material)

    await getMaterials()
    loadingStore.finish()
  }

  function clearForm() {
    editedMaterial.value = JSON.parse(JSON.stringify(initialMaterial))
  }
  return { materials, getMaterials, saveMaterial, deleteMaterial, editedMaterial, getMaterial, clearForm }
})
