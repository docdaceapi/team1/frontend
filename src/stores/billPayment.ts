import { useLoadingStore } from './loading'
import { ref } from 'vue'
import { defineStore } from 'pinia'
import billPaymentService from '@/services/billPayment'
import type { BillPayment } from '@/types/BillPayment'
import { useMessageStore } from './message'

export const useBillPaymentStore = defineStore('billPayment', () => {
  const loadingStore = useLoadingStore()
  const messageStore = useMessageStore()
  const billPayments = ref<BillPayment[]>([])
  const initialBillPayment: BillPayment = {
    id: -1,
    typeBill: { id: 1, name: 'Electricity Bill' },
    total: 0,
    date: '',
    time: ''
  }
  
  const editedBillPayment = ref<BillPayment & { files: File[] }>(JSON.parse(JSON.stringify(initialBillPayment)))

  async function getBillPayment(id: number) {
    try {
        loadingStore.doLoad()
        const res = await billPaymentService.getBillPayment(id)
        editedBillPayment.value = res.data
        loadingStore.finish()
    } catch (e: any) {
        loadingStore.finish()
        messageStore.showMessage(e.message)
    }
  }

  async function getBillPayments() {
    try {
      const res = await billPaymentService.getBillPayments()
      billPayments.value = res.data
      loadingStore.finish()
    } catch (e: any) {
      loadingStore.finish()
      messageStore.showMessage(e.message)
    }
  }

  async function saveBillPayment() {
    try {
        loadingStore.doLoad()
        const billPayment = editedBillPayment.value
        if (!billPayment.id) {
            // Add new
            console.log('Post ' + JSON.stringify(billPayment))
            const res = await billPaymentService.addBillPayment(billPayment)
        } else {
            // Update
            console.log('Patch ' + JSON.stringify(billPayment))
            const res = await billPaymentService.updateBillPayment(billPayment)
        }
        await getBillPayments()
        loadingStore.finish()
    } catch (e: any) {
        messageStore.showMessage(e.message)
        loadingStore.finish()
    }
  }

  async function deleteBillPayment() {
    loadingStore.doLoad()
    const billPayment = editedBillPayment.value
    const res = await billPaymentService.delBillPayment(billPayment)

    await getBillPayments()
    loadingStore.finish()
  }

  function clearForm() {
    editedBillPayment.value = JSON.parse(JSON.stringify(initialBillPayment))
  }
  return { billPayments, getBillPayments, saveBillPayment, deleteBillPayment, editedBillPayment, getBillPayment, clearForm }
})
