import type { Receipt } from '@/types/Receipt'
import http from './http'
import type { ReceiptItem } from '@/types/ReceiptItem'
import { usePaymentStore } from '@/stores/payment'
const paymentStore = usePaymentStore()
type ReceiptDto = {
  orderItems: {
    productId: number
    qty: number
    sweet: string
  }[]
  memberId: number
  userId: number
  memberDiscount: number
  change: number
  paymentType: string
  receivedAmount: number
  total: number
  totalBefore: number
}

function addOrder(receipt: Receipt, receiptItems: ReceiptItem[]) {
  const receiptDto: ReceiptDto = {
    orderItems: [],
    userId: 0,
    memberId: 0,
    memberDiscount: 0,
    change: 0,
    paymentType: '',
    receivedAmount: 0,
    total: 0,
    totalBefore: 0
  }
  receiptDto.memberId = receipt.memberId!
  receiptDto.userId = receipt.userId
  receiptDto.memberDiscount = receipt.memberDiscount
  receiptDto.change = receipt.change
  receiptDto.paymentType = paymentStore.pay
  receiptDto.receivedAmount = receipt.receivedAmount
  receiptDto.total = receipt.total
  receiptDto.totalBefore = receipt.totalBefore || 0
  receiptDto.orderItems = receiptItems.map((item) => {
    return {
      productId: item.productId,
      qty: item.unit,
      sweet: item.sweet
    }
  })
  receiptDto.orderItems = receiptDto.orderItems.filter((item) => item.productId !== undefined)
  return http.post('/orders', receiptDto)
}

function getOrders() {
  return http.get('/orders')
}

function getOrder(id: number) {
  return http.get(`/orders/${id}`)
}

export default { addOrder, getOrders, getOrder }
