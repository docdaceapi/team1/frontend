import type { Material } from '@/types/Material'
import http from './http'


function addMaterial(Material: Material) {
  return http.post('/Materials', Material)
}

function updateMaterial(Material: Material) {
  return http.patch(`/Materials/${Material.id}`, Material)
}

function delMaterial(Material: Material) {
  return http.delete(`/Materials/${Material.id}`)
}

function getMaterial(id: number) {
  return http.get(`/Materials/${id}`)
}

function getMaterials() {
  return http.get('/Materials')
}

export default { addMaterial, updateMaterial, delMaterial, getMaterial, getMaterials }
