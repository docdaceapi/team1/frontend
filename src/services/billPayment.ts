import type { BillPayment } from '@/types/BillPayment'
import http from './http'

function addBillPayment(billPayment: BillPayment & { files: File[] }) {
  const formData = new FormData()
  formData.append('typeBill', JSON.stringify(billPayment.typeBill))
  formData.append('total', billPayment.total.toString())
  formData.append('date', JSON.stringify(billPayment.date))
  formData.append('time', JSON.stringify(billPayment.time))
  if (billPayment.files && billPayment.files.length > 0) {
    formData.append('file', billPayment.files[0])
    console.log(billPayment.files[0])
  }
    return http.post('/billPayments', formData, {
        headers: {
            'Content-BillPayment': 'multipart/form-data'
        }
    })
}

function updateBillPayment(billPayment: BillPayment & { files: File[] }) {
  const formData = new FormData()
  formData.append('typeBill', JSON.stringify(billPayment.typeBill))
  formData.append('total', billPayment.total.toString())
  formData.append('date', JSON.stringify(billPayment.date))
  formData.append('time', JSON.stringify(billPayment.time))
  if (billPayment.files && billPayment.files.length > 0) {
    formData.append('file', billPayment.files[0])
    console.log(billPayment.files[0])
  }
    return http.post('/billPayments', formData, {
        headers: {
            'Content-BillPayment': 'multipart/form-data'
        }
    })
}

function delBillPayment(billPayment: BillPayment) {
  return http.delete(`/billPayments/${billPayment.id}`)
}

function getBillPayment(id: number) {
  return http.get(`/billPayments/${id}`)
}

function getBillPaymentsByType(typeId: number) {
  return http.get('/billPayments/type/' + typeId)
}

function getBillPayments() {
  return http.get('/billPayments')
}

export default { addBillPayment, updateBillPayment, delBillPayment, getBillPayment, getBillPayments, getBillPaymentsByType }
